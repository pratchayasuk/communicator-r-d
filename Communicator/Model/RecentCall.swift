//
//  RecentCall.swift
//  Communicator
//
//  Created by Pratchaya Suksena on 20/10/2564 BE.
//

import Foundation

struct RecentCall: Identifiable {
    
    let id: Int
    var name: String
    var time: String
    var lastMessage: String
    
}
