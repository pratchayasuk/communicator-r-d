//
//  CommunicatorApp.swift
//  Communicator
//
//  Created by Pratchaya Suksena on 18/10/2564 BE.
//

import SwiftUI

@main
struct CommunicatorApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
