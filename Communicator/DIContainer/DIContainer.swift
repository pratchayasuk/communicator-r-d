//
//  DIContainer.swift
//  Communicator
//
//  Created by Pratchaya Suksena on 18/10/2564 BE.
//

import Foundation

class DIContainer {
    
    // MARK: - ViewModel
    static func makeMainViewModel(viewState: MainViewModel.ViewState = .onAppear,
                                  isAllowContact: Bool = false,
                                  currentTab: MainViewModel.TabItem = .trueIDCall) -> MainViewModel {
        MainViewModel(viewState: viewState,
                      isAllowContact: isAllowContact,
                      currentTab: currentTab)
    }
    
    static func makeRecentCallViewModel(viewState: RecentCallViewModel.ViewState = .onAppear,
                                        bannerState: RecentCallViewModel.BannerState = .notRedeem,
                                        recentCalls: [RecentCall] = [],
                                        isCanCall: Bool = false) -> RecentCallViewModel {
        RecentCallViewModel(viewState: viewState,
                            bannerState: bannerState,
                            recentCalls: recentCalls,
                            isCanCall: isCanCall)
    }
    
    static func makeRecentCallRowViewModel(recentCall: RecentCall = RecentCall(id: 1,
                                                                               name: "Mock Name",
                                                                               time: "Mock Time",
                                                                               lastMessage: "Mock LastMessage")) -> RecentCallRowViewModel {
        RecentCallRowViewModel(recentCall: recentCall)
    }
    
    // MARK: - UseCase
    
    
    // MARK: - Repository
    
    
}
