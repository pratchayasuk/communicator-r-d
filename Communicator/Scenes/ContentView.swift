//
//  ContentView.swift
//  Communicator
//
//  Created by Pratchaya Suksena on 18/10/2564 BE.
//

import SwiftUI

struct ContentView: View {
    
    @State private var isPresented: Bool = false
    
    var body: some View {
        Text("Tap me to open Call & Chat")
            .onTapGesture {
                isPresented.toggle()
            }
            .fullScreenCover(isPresented: $isPresented) {
                debugPrint("On Dismiss MainView")
            } content: {
                MainView(viewModel: DIContainer.makeMainViewModel())
            }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
