//
//  MainView.swift
//  Communicator
//
//  Created by Pratchaya Suksena on 18/10/2564 BE.
//

import SwiftUI

struct MainView: View {
    
    @Environment(\.presentationMode) private var presentationMode
    @ObservedObject var viewModel: MainViewModel
    
    var body: some View {
        NavigationView {
            content()
                .navigationTitle("Call & Chat")
                .toolbar {
                    mainToolbar()
                }
                .foregroundColor(.black)
                .preferredColorScheme(.light)
        }
    }
    
    @ToolbarContentBuilder
    func mainToolbar() -> some ToolbarContent {
        ToolbarItem(placement: .navigationBarLeading) {
            Button {
                presentationMode.wrappedValue.dismiss()
            } label: {
                Image(systemName: "chevron.left")
            }
        }
        
        ToolbarItemGroup(placement: .navigationBarTrailing) {
            if viewModel.viewState == .loaded && viewModel.isAllowContact {
                IconCircle(circleColor: .gray,
                           opcacity: 0.2,
                           size: 32,
                           image: Image(systemName: "book.closed"),
                           imageColor: .black)
                    .onTapGesture {
                        presentationMode.wrappedValue.dismiss()
                    }
                
                IconCircle(circleColor: .gray,
                           opcacity: 0.2,
                           size: 32,
                           image: Image(systemName: "gear"),
                           imageColor: .black)
                    .onTapGesture {
                        presentationMode.wrappedValue.dismiss()
                    }
            } else {
                EmptyView()
            }
        }
    }
    
    @ViewBuilder
    func content() -> some View {
        switch viewModel.viewState {
        case .onAppear:
            Color.clear
                .onAppear(perform: {
                    viewModel.checkFeatureCondition()
                })
        case .loading:
            ProgressView()
        case .loaded:
            if viewModel.isAllowContact {
                ScrollView {
                    VStack {
                        // Search View
                        HStack {
                            Image(systemName: "magnifyingglass")
                            
                            Text("Search in TrueID Call & Chat")
                                .foregroundColor(.gray)
                            
                            Spacer()
                        }
                        .padding(EdgeInsets(top: 25, leading: 30, bottom: 25, trailing: 30))
                        .onTapGesture {
                            debugPrint("tap search")
                        }
                        .overlay(
                            RoundedRectangle(cornerRadius: 30)
                                .stroke(lineWidth: 2)
                                .foregroundColor(.gray)
                                .padding(15)
                        )
                        
                        // Header Tab View
                        HStack {
                            Button {
                                viewModel.setSeletedTab(tab: .trueIDCall)
                            } label: {
                                Text("TrueID Call")
                                    .padding([.bottom, .horizontal])
                                    .overlay(EdgeBorder(width: 5, edges: [.bottom])
                                                .isHidden(!(viewModel.currentTab == .trueIDCall)))
                                    .foregroundColor(.black)
                            }
                            
                            Button {
                                viewModel.setSeletedTab(tab: .trueIDChat)
                            } label: {
                                Text("TrueID Chat")
                                    .padding([.bottom, .horizontal])
                                    .overlay(EdgeBorder(width: 5, edges: [.bottom])
                                                .isHidden(!(viewModel.currentTab == .trueIDChat)))
                                    .foregroundColor(.black)
                            }
                            
                            Spacer()
                        }
                        .padding(EdgeInsets(top: 0, leading: 15, bottom: 0, trailing: 15))
                        
                        // Content Tab View
                        if viewModel.currentTab == .trueIDCall {
                            RecentCallView(viewModel: DIContainer.makeRecentCallViewModel())
                        } else {
                            RecentCallView(viewModel: DIContainer.makeRecentCallViewModel())
                        }
                    }
                }
            } else {
                VStack {
                    Image(systemName: "person.crop.artframe")
                        .resizable()
                        .frame(width: 300, height: 300, alignment: .center)
                        .scaledToFit()
                    
                    Button {
                        viewModel.requestContactPermission()
                    } label: {
                        Text("Allow Contact")
                    }
                }
            }
        case .notAllowContact:
            VStack {
                Image(systemName: "person.crop.artframe")
                Button {
                    
                } label: {
                    Text("Allow Contact again")
                }
            }
        }
    }
    
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView(viewModel: DIContainer.makeMainViewModel(viewState: .loaded, isAllowContact: false, currentTab: .trueIDCall))
    }
}
