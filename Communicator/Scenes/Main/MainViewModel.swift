//
//  MainViewModel.swift
//  Communicator
//
//  Created by Pratchaya Suksena on 18/10/2564 BE.
//

import Foundation

class MainViewModel: ObservableObject {
    
    enum TabItem {
        case trueIDCall
        case trueIDChat
    }
    
    enum ViewState {
        case onAppear
        case loading
        case loaded
        case notAllowContact
    }
    
    @Published var viewState: ViewState
    @Published var isAllowContact: Bool
    @Published var currentTab: TabItem
    
    init(viewState: ViewState, isAllowContact: Bool, currentTab: TabItem) {
        self.viewState = viewState
        self.isAllowContact = isAllowContact
        self.currentTab = currentTab
    }
    
    func setSeletedTab(tab: TabItem) {
        currentTab = tab
    }
    
    func requestContactPermission() {
        viewState = .loading
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            self.isAllowContact = true
            self.viewState = .loaded
        }
    }
    
    func checkFeatureCondition() {
        viewState = .loading
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            self.viewState = .loaded
        }
    }
    
    private func fetchFeatureConfig() {
        
    }
    
    private func fetchInitialApp() {
        
    }
    
}
