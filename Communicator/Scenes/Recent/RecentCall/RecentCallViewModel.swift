//
//  RecentCallViewModel.swift
//  Communicator
//
//  Created by Pratchaya Suksena on 21/10/2564 BE.
//

import Foundation

class RecentCallViewModel: ObservableObject {
    
    enum ViewState {
        case onAppear
        case loaded
        case loading
    }
    
    enum BannerState {
        case notRedeem
        case waitRedeem
        case redeemSuccess
        case redeemFail
    }
    
    @Published var recentCalls: [RecentCall]
    @Published var viewState: ViewState
    @Published var bannerState: BannerState
    @Published var isCanCall: Bool
    
    init(viewState: ViewState, bannerState: BannerState, recentCalls: [RecentCall], isCanCall: Bool) {
        self.viewState = viewState
        self.bannerState = bannerState
        self.recentCalls = recentCalls
        self.isCanCall = isCanCall
    }
    
    func redeemQuota() {
        if bannerState == .waitRedeem || bannerState == .redeemSuccess { return }
        bannerState = .waitRedeem
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            self.bannerState = .redeemSuccess
        }
    }
    
    func fetchRecentCall() {
        viewState = .loading
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            var recentCalls: [RecentCall] = []
            for id in 1...15 {
                recentCalls.append(RecentCall(id: id, name: "023456789", time: "4:58 PM", lastMessage: "Call ended: 52s"))
            }
            self.recentCalls = recentCalls
            self.viewState = .loaded
        }
    }
    
}

