//
//  RecentCallRowViewModel.swift
//  Communicator
//
//  Created by Pratchaya Suksena on 21/10/2564 BE.
//

import Foundation

class RecentCallRowViewModel: ObservableObject {
    
    @Published var recentCall: RecentCall
    
    init(recentCall: RecentCall) {
        self.recentCall = recentCall
    }
    
}
