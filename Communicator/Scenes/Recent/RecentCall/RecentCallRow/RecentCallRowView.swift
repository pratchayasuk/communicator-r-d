//
//  RecentCallRowView.swift
//  Communicator
//
//  Created by Pratchaya Suksena on 21/10/2564 BE.
//

import SwiftUI

struct RecentCallRowView: View {
    
    @ObservedObject var viewModel: RecentCallRowViewModel
    
    var body: some View {
        HStack(alignment: .center, spacing: 20, content: {
            IconCircle(circleColor: .gray,
                       opcacity: 0.2,
                       size: 32,
                       image: Image(systemName: "phone.fill"),
                       imageColor: .black)
            
            VStack(alignment: .leading, spacing: nil) {
                Text(viewModel.recentCall.name)
                    .font(.headline)
                
                Text(viewModel.recentCall.lastMessage)
                    .font(.subheadline)
            }
            
            Spacer()
            
            Text(viewModel.recentCall.time)
                .font(.body)
        })
            .padding(.horizontal)
    }
}

struct RecentCallRowView_Previews: PreviewProvider {
    static var previews: some View {
        RecentCallRowView(viewModel: DIContainer.makeRecentCallRowViewModel())
            .previewLayout(.sizeThatFits)
    }
}
