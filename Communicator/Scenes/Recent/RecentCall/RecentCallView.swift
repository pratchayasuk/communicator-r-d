//
//  RecentCallView.swift
//  Communicator
//
//  Created by Pratchaya Suksena on 21/10/2564 BE.
//

import SwiftUI

struct RecentCallView: View {
    
    @ObservedObject var viewModel: RecentCallViewModel
    
    var body: some View {
        ZStack {
            ScrollView {
                LazyVStack {
                    // Banner View
                    bannerView()
                    
                    // Recent Call View
                    switch viewModel.viewState {
                    case .onAppear:
                        Color.clear
                            .onAppear {
                                viewModel.fetchRecentCall()
                            }
                    case .loading:
                        ProgressView()
                    case .loaded:
                        ForEach(viewModel.recentCalls) { recentCall in
                            RecentCallRowView(viewModel: RecentCallRowViewModel(recentCall: recentCall))
                            Divider()
                        }
                    }
                    
                }
            }
        }
    }
    
    func bannerView() -> some View {
        var title: String = ""
        var subTitle1: String = ""
        var subTitle2: String = ""
        var status: String = ""
        
        switch viewModel.bannerState {
        case .notRedeem:
            title = "Free call to phone no."
            subTitle1 = "No data charge 60 mins./month"
            subTitle2 = "(Only True Move H no.)"
            status = "Get Started!"
        case .waitRedeem:
            title = "Free call to phone no."
            subTitle1 = "No data charge 60 mins./month"
            subTitle2 = "(Only True Move H no.)"
            status = "Wating..."
        case .redeemSuccess:
            title = "Free call to phone no."
            subTitle1 = "No data charge 60 mins./month"
            subTitle2 = "(Only True Move H no.)"
            status = "Success..."
        case .redeemFail:
            title = "Free call to phone no."
            subTitle1 = "No data charge 60 mins./month"
            subTitle2 = "(Only True Move H no.)"
            status = "Redeem again"
        }
        
        return AnyView(
            HStack {
                IconCircle(circleColor: .black, opcacity: 1.0, size: 32, image: Image(systemName: "phone.fill"), imageColor: .white)
                
                VStack(alignment: .leading, spacing: nil, content: {
                    Text(title)
                        .font(.system(size: 12))
                        .fontWeight(.bold)
                    
                    Text(subTitle1)
                        .font(.system(size: 10))
                        .foregroundColor(.gray)
                    
                    Text(subTitle2)
                        .font(.system(size: 10))
                        .foregroundColor(.gray)
                })
                
                Spacer()
                
                Button {
                    viewModel.redeemQuota()
                } label: {
                    Text(status)
                        .foregroundColor(.white)
                        .font(.system(size: 14))
                        .fontWeight(.bold)
                }
                .padding(7)
                .background(Color.black)
                .clipShape(Capsule())
                
            }
                .padding()
                .background(Color.gray.opacity(0.1))
        )
        
    }
    
}

struct RecentCallView_Previews: PreviewProvider {
    static var previews: some View {
        var recentCalls: [RecentCall] = []
        for id in 1...15 {
            recentCalls.append(RecentCall(id: id, name: "023456789", time: "4:58 PM", lastMessage: "Call ended: 52s"))
        }
        return RecentCallView(viewModel: DIContainer.makeRecentCallViewModel(viewState: .onAppear,
                                                                             bannerState: .notRedeem,
                                                                             recentCalls: recentCalls,
                                                                             isCanCall: false))
    }
}
