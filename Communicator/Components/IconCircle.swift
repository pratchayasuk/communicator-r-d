//
//  IconCircle.swift
//  Communicator
//
//  Created by Pratchaya Suksena on 21/10/2564 BE.
//

import SwiftUI

struct IconCircle: View {
    
    var circleColor: Color
    var opcacity: CGFloat
    var size: CGFloat
    var image: Image
    var imageColor: Color
    
    var body: some View {
        ZStack {
            circleColor
                .opacity(opcacity)
                .clipShape(Circle())
                .frame(width: size, height: size, alignment: .center)
            
            image
                .resizable()
                .scaledToFit()
                .frame(width: (size/2), height: (size/2), alignment: .center)
                .foregroundColor(imageColor)
        }
    }
}

struct IconCircle_Previews: PreviewProvider {
    static var previews: some View {
        IconCircle(circleColor: .gray, opcacity: 0.3, size: 32, image: Image(systemName: "phone.fill"), imageColor: .black)
            .previewLayout(.sizeThatFits)
    }
}
