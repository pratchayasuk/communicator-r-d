//
//  View + Extension.swift
//  Communicator
//
//  Created by Pratchaya Suksena on 20/10/2564 BE.
//

import SwiftUI

extension View {
    
    func isHidden(_ isHidden: Bool) -> some View {
        self.opacity(isHidden ? 0.0 : 1.0)
    }
    
    func border(width: CGFloat, color: Color, edges: [Edge]) -> some View {
        self.overlay(EdgeBorder(width: width, edges: edges)).foregroundColor(color)
    }
    
}
